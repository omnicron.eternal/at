package com.nmu.yearproject;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomArrayAdapterModule extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<Module> modules;
    private final int mResource;

    public CustomArrayAdapterModule(@NonNull Context context, @LayoutRes int resource,
                              @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        modules = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView moduleCode = (TextView) view.findViewById(R.id.ModuleCodeSpinMod);
        TextView moduleName = (TextView) view.findViewById(R.id.ModuleName);
        TextView moduleFaculty = (TextView) view.findViewById(R.id.ModuleFaculty);

        Module mod = modules.get(position);

        moduleCode.setText(mod.getModuleCode());
        moduleName.setText(mod.getModuleName());
        moduleFaculty.setText(mod.getModuleFaculty());

        return view;
    }
}
