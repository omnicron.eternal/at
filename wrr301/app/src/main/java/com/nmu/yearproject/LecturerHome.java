package com.nmu.yearproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LecturerHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecturer_home_matt);
        //Button refreshdata=(Button) findViewById(R.id.button14);
        //refreshdata.setVisibility(View.GONE);

    }
    public void maintainModule(View view) {
        Intent intent = new Intent(this, MaintainModule.class);
        Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");
        intent.putExtra("EXTRA_STUDENT",stu);
        startActivity(intent);
    }
    public void takeAttendance(View view) {
        Intent intent = new Intent(this, TakeAttendance.class);
        startActivity(intent);
    }

    public void viewSessionAttendance(View view) {
        Intent intent = new Intent(this, ViewSessionRecord.class);
        Student stu = (Student) getIntent().getSerializableExtra("EXTRA_STUDENT");
        intent.putExtra("EXTRA_STUDENT", stu);
        startActivity(intent);
    }
    public void maintainSessionAttendance(View view) {
        Intent intent = new Intent(this, MaintainSessionLect.class);
        Student stu = (Student) getIntent().getSerializableExtra("EXTRA_STUDENT");
        intent.putExtra("EXTRA_STUDENT", stu);
        startActivity(intent);
    }
    public void viewAdministrationDetails(View view) {
        Intent intent = new Intent(this, ViewAdministrationDetails.class);
        Student stu = (Student) getIntent().getSerializableExtra("EXTRA_STUDENT");
        intent.putExtra("EXTRA_STUDENT", stu);
        startActivity(intent);
    }
    public void viewTrends(View view) {
        Intent intent = new Intent(this, ViewTrends.class);
        Student stu = (Student) getIntent().getSerializableExtra("EXTRA_STUDENT");
        intent.putExtra("EXTRA_STUDENT", stu);
        startActivity(intent);
    }

}
