package com.nmu.yearproject;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_matt);

        Student dylanPhelps =new Student("216233046","Dylan","Phelps","s216233046@mandela.ac.za","123",true,false);
        Student mattWatson =new Student("215100328","Matt","Watson","s215100328@mandela.ac.za","abc",false,true);
        Student isabelleTaljaard =new Student("216087686","Isabelle","Taljaard","s216087686@mandela.ac.za","123",false,false);
        Student adamKhan =new Student("216302420","Adam","Khan","s216302420@mandela.ac.za","abc",false,false);


        Module matt302=new Module("MATT302","Modern Algebra","Study of abstract algebraic structures","Science");
        Module wrlv301=new Module("WRLV301","Formal Language and Automata Theory","FLAT","Science");
        Module wrui301=new Module("WRUI301","User Interface Design","UI Design","Science");

        Session matt302a1=new Session("MATT302001","MATT302","07:45","09:05","2018/09/11","0702025",Boolean.TRUE,"YES");
        Session wrlv301b2=new Session("WRLV301123","WRLV301","10:25","11:45","2018/08/19","3001017",Boolean.TRUE,"NO");
        Session wrui301100=new Session("WRUI301100","WRUI301","09:05","10:25","2018/06/27","Lab 4",Boolean.TRUE,"NO");
        Session matt302a2=new Session("MATT302002","MATT302","14:05","15:15","2018/03/15","3001037",Boolean.TRUE,"NO");
        Session matt302a3=new Session("MATT302003","MATT302","11:45","13:55","2018/10/30","3001037",Boolean.TRUE,"NO");



        StudentDao studentDao=appDatabase.getDatabase(getBaseContext()).studentDao();
        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();
        SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();
        StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(getBaseContext()).studentSessionJoinDao();
        StudentModuleJoinDao studentModuleJoinDao=appDatabase.getDatabase(getBaseContext()).studentModuleJoinDao();

        /*
        moduleDao.deleteAll();
        sessionDao.deleteAll();
        studentDao.deleteAll();
        studentSessionJoinDao.deleteAll();
        studentModuleJoinDao.deleteAll();
        */
        studentDao.insert(dylanPhelps,mattWatson,isabelleTaljaard,adamKhan);
        moduleDao.insert(matt302,wrlv301,wrui301);
        sessionDao.insert(matt302a1,wrlv301b2,wrui301100,matt302a2,matt302a3);

        studentModuleJoinDao.insert(new StudentModuleJoin("216233046","MATT302"));
        studentModuleJoinDao.insert(new StudentModuleJoin("216233046","WRLV301"));
        studentModuleJoinDao.insert(new StudentModuleJoin("216233046","WRUI301"));

        studentModuleJoinDao.insert(new StudentModuleJoin("215100328","WRUI301"));
        studentModuleJoinDao.insert(new StudentModuleJoin("215100328","MATT302"));


        studentSessionJoinDao.insert(new StudentSessionJoin("216233046","MATT302001"));
        studentSessionJoinDao.insert(new StudentSessionJoin("216233046","WRLV301123"));

        studentSessionJoinDao.insert(new StudentSessionJoin("215100328","MATT302001"));
        studentSessionJoinDao.insert(new StudentSessionJoin("215100328","WRUI301100"));



    }

    public void login(View view)
    {
        EditText sNum=(EditText) findViewById(R.id.editTextStuNum);
        EditText pass=(EditText) findViewById(R.id.editTextPassword);
        TextView error=(TextView) findViewById(R.id.txt_loginError);
        String studentNum=sNum.getText().toString();
        String password=pass.getText().toString();
        StudentDao studentDao = appDatabase.getDatabase(context).studentDao();

        Student stu =studentDao.findStudentByStudentNum(studentNum);
        if (stu==null)
        {
            error.setText("No user found for that student number");
        }
        else if (!password.equals(stu.getPassword()))
        {
            error.setText("Wrong password");
        }
        else
        {
            if (stu.getLecturer())
            {
                Intent intent = new Intent(this, LecturerHome.class);
                intent.putExtra("EXTRA_STUDENT",stu);
                startActivity(intent);
            }
            else
            {
                Intent intent = new Intent(this, StudentHome.class);
                intent.putExtra("EXTRA_STUDENT",stu);
                startActivity(intent);
            }

        }
    }

    public void register(View view) {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }

    public void resetDatabase(View view)
    {
        Student dylanPhelps =new Student("216233046","Dylan","Phelps","s216233046@mandela.ac.za","123",true,false);
        Student mattWatson =new Student("215100328","Matt","Watson","s215100328@mandela.ac.za","abc",false,true);
        Student isabelleTaljaard =new Student("216087686","Isabelle","Taljaard","s216087686@mandela.ac.za","123",false,false);
        Student adamKhan =new Student("216302420","Adam","Khan","s216302420@mandela.ac.za","abc",false,false);


        Module matt302=new Module("MATT302","Modern Algebra","Study of abstract algebraic structures","Science");
        Module wrlv301=new Module("WRLV301","Formal Language and Automata Theory","FLAT","Science");
        Module wrui301=new Module("WRUI301","User Interface Design","UI Design","Science");

        Session matt302a1=new Session("MATT302001","MATT302","07:45","09:05","2018/09/11","0702025",Boolean.TRUE,"YES");
        Session wrlv301b2=new Session("WRLV301123","WRLV301","10:25","11:45","2018/08/19","3001017",Boolean.TRUE,"NO");
        Session wrui301100=new Session("WRUI301100","WRUI301","09:05","10:25","2018/06/27","Lab 4",Boolean.TRUE,"NO");
        Session matt302a2=new Session("MATT302002","MATT302","14:05","15:15","2018/03/15","3001037",Boolean.TRUE,"NO");
        Session matt302a3=new Session("MATT302003","MATT302","11:45","13:55","2018/10/30","3001037",Boolean.TRUE,"NO");



        StudentDao studentDao=appDatabase.getDatabase(getBaseContext()).studentDao();
        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();
        SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();
        StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(getBaseContext()).studentSessionJoinDao();
        StudentModuleJoinDao studentModuleJoinDao=appDatabase.getDatabase(getBaseContext()).studentModuleJoinDao();


        moduleDao.deleteAll();
        sessionDao.deleteAll();
        studentDao.deleteAll();
        studentSessionJoinDao.deleteAll();
        studentModuleJoinDao.deleteAll();

        studentDao.insert(dylanPhelps,mattWatson,isabelleTaljaard,adamKhan);
        moduleDao.insert(matt302,wrlv301,wrui301);
        sessionDao.insert(matt302a1,wrlv301b2,wrui301100,matt302a2,matt302a3);

        studentModuleJoinDao.insert(new StudentModuleJoin("216233046","MATT302"));
        studentModuleJoinDao.insert(new StudentModuleJoin("216233046","WRLV301"));
        studentModuleJoinDao.insert(new StudentModuleJoin("216233046","WRUI301"));

        studentModuleJoinDao.insert(new StudentModuleJoin("215100328","WRUI301"));
        studentModuleJoinDao.insert(new StudentModuleJoin("215100328","MATT302"));


        studentSessionJoinDao.insert(new StudentSessionJoin("216233046","MATT302001"));
        studentSessionJoinDao.insert(new StudentSessionJoin("216233046","WRLV301123"));

        studentSessionJoinDao.insert(new StudentSessionJoin("215100328","MATT302001"));
        studentSessionJoinDao.insert(new StudentSessionJoin("215100328","WRUI301100"));


    }
}
