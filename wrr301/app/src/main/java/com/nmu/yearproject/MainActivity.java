package com.nmu.yearproject;

        import android.app.NotificationChannel;
        import android.app.NotificationManager;
        import android.app.PendingIntent;
        import android.content.Context;
        import android.content.Intent;
        import android.support.v4.app.NotificationCompat;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showNotification("Test","Hello World");


        Student dylanPhelps =new Student("216233046","Dylan","Phelps","s216233046@mandela.ac.za","123",true,false);
        Student mattWatson =new Student("215100328","Matt","Watson","s215100328@mandela.ac.za","abc",false,true);
        Student isabelleTaljaard =new Student("216087686","Isabelle","Taljaard","s216087686@mandela.ac.za","123",false,false);
        Student adamKhan =new Student("216302420","Adam","Khan","s216302420@mandela.ac.za","abc",false,false);


        Module matt302=new Module("MATT302","Modern Algebra","Study of abstract algebraic structures","Science");
        Module wrlv301=new Module("WRLV301","Formal Language and Automata Theory","FLAT","Science");
        Module wrui301=new Module("wrui301","User Interface Design","UI Design","Science");

        Session matt302a1=new Session("MATT302001","MATT302","07:45","09:05","2018/09/11","0702025",Boolean.TRUE,"YES");
        Session wrlv301b2=new Session("WRLV301123","WRLV301","10:25","11:45","2018/08/19","3001017",Boolean.TRUE,"NO");
        Session wrui301100=new Session("WRUI301100","wrui301","09:05","10:25","2018/06/27","Lab 4",Boolean.TRUE,"NO");


        StudentDao studentDao=appDatabase.getDatabase(getBaseContext()).studentDao();
        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();
        SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();
        StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(getBaseContext()).studentSessionJoinDao();

        studentDao.insert(dylanPhelps,mattWatson,isabelleTaljaard,adamKhan);
        moduleDao.insert(matt302,wrlv301,wrui301);
        sessionDao.insert(matt302a1,wrlv301b2);
        studentSessionJoinDao.insert(new StudentSessionJoin("216233046","MATT302001"));
        studentSessionJoinDao.insert(new StudentSessionJoin("216233046","WRLV301123"));
        studentSessionJoinDao.insert(new StudentSessionJoin("215100328","MATT302001"));
    }
    /*
    public void register(View view) {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }
*/
/*
    public void maintainModule(View view) {
        Intent intent = new Intent(this, MaintainModule.class);
        startActivity(intent);
    }
*/
/*
    public void maintainSession(View view) {
        Intent intent = new Intent(this, MaintainSession.class);
        startActivity(intent);
    }
*/
/*
    public void takeAttendance(View view) {
        Intent intent = new Intent(this, TakeAttendance.class);
        startActivity(intent);
    }
*/
    public void viewAttendanceRecord(View view) {
        final Intent intent = new Intent(this, MaintainRecord.class);
        startActivity(intent);
    }
    public void viewAdministrationDetails(View view) {
        final Intent intent = new Intent(this, ViewAdministrationDetails.class);
        startActivity(intent);
    }

    void showNotification(String title, String content) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "YOUR_CHANNEL_NAME", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DISCRIPTION");
            mNotificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(content)// message for notification
                //.setSound(alarmSound) // set alarm sound for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        mNotificationManager.notify(0, mBuilder.build());
    }
}
