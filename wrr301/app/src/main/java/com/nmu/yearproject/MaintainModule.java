package com.nmu.yearproject;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import static com.nmu.yearproject.ModuleSaveDialogFragment.TAG_DIALOG_MODULE_SAVE;

public class MaintainModule extends AppCompatActivity {
    Context context;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintain_module);
        context=this;

        final ListView modListView= (ListView) findViewById(R.id.listView_Module);
        StudentModuleJoinDao studentModuleJoinDao=appDatabase.getDatabase(getBaseContext()).studentModuleJoinDao();

        Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");

        final List<Module> moduleList=studentModuleJoinDao.getModuleForStudentStatic(stu.getStudentNumber());

        final ModuleListAdapterStatic adapter = new ModuleListAdapterStatic(this,R.layout.adapter_module,moduleList);
        modListView.setAdapter(adapter);

        Button btnAddModule=(Button) findViewById(R.id.btn_addModuleMaintain) ;
        btnAddModule.setText("Add Module");
        modListView.invalidate();

        btnAddModule.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //ContextThemeWrapper ctw=new ContextThemeWrapper(context,R.style)
                AlertDialog.Builder alert = new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert);

                final EditText edt_ModuleCode = new EditText(context);
                edt_ModuleCode.setHint("ModuleCode");
                final EditText edt_ModuleName = new EditText(context);
                edt_ModuleName.setHint("Module Name");
                final EditText edt_ModuleFaculty = new EditText(context);
                edt_ModuleFaculty.setHint("ModuleFaculty");
                final EditText edt_ModuleDescription = new EditText(context);
                edt_ModuleDescription.setHint("ModuleDescription");

                //alert.setMessage("Enter Your Message");
                alert.setTitle("Enter Module");

                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                layout.addView(edt_ModuleCode);
                layout.addView(edt_ModuleName);
                layout.addView(edt_ModuleFaculty);
                layout.addView(edt_ModuleDescription);


                alert.setView(layout);



                alert.setPositiveButton("Create Module", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Module module=new Module(edt_ModuleCode.getText().toString(),edt_ModuleName.getText().toString(),edt_ModuleDescription.getText().toString(),edt_ModuleFaculty.getText().toString());
                        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();
                        moduleDao.insert(module);

                        adapter.add(module);
                        adapter.notifyDataSetChanged();
                        //modListView.refreshDrawableState();
                        modListView.invalidate();

                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                    }
                });

                alert.show();
            }
        });
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        modListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert)
                        .setTitle("Delete Record")
                        .setMessage("Are you sure you want to delete this record")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                String ModuleCode=((Module)modListView.getItemAtPosition(position)).getModuleCode();

                                ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();
                                Module mod =moduleDao.findModuleByCode(ModuleCode);

                                moduleDao.delete(mod);


                                adapter.remove(mod);
                                adapter.notifyDataSetChanged();

                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });



    }
/*
    private Fragment shownFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintain_module);
        initView();
        if (savedInstanceState == null) {
            showFragment(ModuleListFragment.newInstance());
        }

    }

    private void initView() {
        showFragment(ModuleListFragment.newInstance());

        Button btn_addModule = findViewById(R.id.btn_addModule);
        btn_addModule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveDialog();
            }
        });
    }
    private void showFragment(final Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentHolderModule, fragment);
        fragmentTransaction.commitNow();
        shownFragment = fragment;
    }
    private void showSaveDialog() {
        DialogFragment dialogFragment;
        String tag;

        dialogFragment = ModuleSaveDialogFragment.newInstance(null, null,null,null);
        tag = TAG_DIALOG_MODULE_SAVE;
        dialogFragment.show(getSupportFragmentManager(), tag);
    }

*/
}
