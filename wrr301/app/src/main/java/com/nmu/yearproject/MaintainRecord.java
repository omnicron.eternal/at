package com.nmu.yearproject;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import static com.nmu.yearproject.StudentSessionJoinSaveDialogFragment.TAG_DIALOG_STUDENTSESSIONJOIN_SAVE;

public class MaintainRecord extends AppCompatActivity {

    private Fragment shownFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintain_record);
        initView();
        if (savedInstanceState == null) {
            showFragment(StudentSessionJoinListFragment.newInstance());
        }

    }

    private void initView() {
        showFragment(StudentSessionJoinListFragment.newInstance());

        Button btn_addRecord = findViewById(R.id.btn_addRecord);

        btn_addRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveDialog();
            }
        });

    }

    private void showFragment(final Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentHolderRecord, fragment);
        fragmentTransaction.commitNow();
        shownFragment = fragment;
    }


    private void showSaveDialog() {
        DialogFragment dialogFragment;
        String tag;

        dialogFragment = StudentSessionJoinSaveDialogFragment.newInstance(null, null);
        tag = TAG_DIALOG_STUDENTSESSIONJOIN_SAVE;
        dialogFragment.show(getSupportFragmentManager(), tag);
    }


}
