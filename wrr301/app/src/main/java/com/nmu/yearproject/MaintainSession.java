package com.nmu.yearproject;

import android.arch.lifecycle.LiveData;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import static com.nmu.yearproject.ModuleSaveDialogFragment.TAG_DIALOG_MODULE_SAVE;
import static com.nmu.yearproject.SessionSaveDialogFragment.TAG_DIALOG_SESSION_SAVE;

public class MaintainSession extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintain_session);
        ListView sesListView= (ListView) findViewById(R.id.listView_Session);
        StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(getBaseContext()).studentSessionJoinDao();

        Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");

        //StudentSessionJoin stuJoin=studentSessionJoinDao.findStudentSessionJoin(stu.getStudentNumber(),"MATT302001");

        //List<StudentSessionJoin>stuJoinList =studentSessionJoinDao.getAll().getValue();
        //LiveData<List<Session>> sessionListLive=studentSessionJoinDao.getSessionForStudent(stu.getStudentNumber());
        List<Session> sessionList=studentSessionJoinDao.getSessionForStudentStatic(stu.getStudentNumber());

        SessionListAdapterStatic adapter = new SessionListAdapterStatic(this,R.layout.adapter_session,sessionList);
        sesListView.setAdapter(adapter);
        if (sesListView.getHeaderViewsCount()==0)
        {
            View headerView = getLayoutInflater().inflate(R.layout.header_session, null);
            sesListView.addHeaderView(headerView);
        }



    }
    }
/*
    private Fragment shownFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintain_session);
        initView();
        if (savedInstanceState == null) {
            showFragment(SessionListFragment.newInstance());
        }

    }

    private void initView() {
        showFragment(SessionListFragment.newInstance());

        Button btn_addSesison = findViewById(R.id.btn_addSession);
        btn_addSesison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveDialog();
            }
        });
    }
    private void showFragment(final Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentHolderSession, fragment);
        fragmentTransaction.commitNow();
        shownFragment = fragment;
    }
    private void showSaveDialog() {
        DialogFragment dialogFragment;
        String tag;

        dialogFragment = SessionSaveDialogFragment.newInstance(null, null,null,null,null,null,null,null);
        tag = TAG_DIALOG_SESSION_SAVE;
        dialogFragment.show(getSupportFragmentManager(), tag);
    }
    */

