package com.nmu.yearproject;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

public class MaintainSessionLect extends AppCompatActivity {

    Context context;
    SessionListAdapterStatic adapterSession;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.activity_maintain_session_lect);

        final Spinner spin=(Spinner) findViewById(R.id.spn_selectModule);

        Button btn_addRecord=(Button)findViewById(R.id.button4);


        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();

        final Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");

        List<Module> moduleList=moduleDao.getAllStatic();

        CustomArrayAdapterModule adapter = new CustomArrayAdapterModule(this,R.layout.adapter_spinner_module,moduleList);
        spin.setAdapter(adapter);




        spin.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, final int pos, long id) {

                        final ListView sesListView= (ListView) findViewById(R.id.listView_sessionSelectByModule);
                        final SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();

                        final List<Session> sessions =sessionDao.findSessionByModuleCode(((Module)spin.getSelectedItem()).getModuleCode());

                        adapterSession = new SessionListAdapterStatic(context,R.layout.adapter_session,sessions);
                        sesListView.setAdapter(adapterSession);

                        if (sesListView.getHeaderViewsCount()==0)
                        {
                            View headerView = getLayoutInflater().inflate(R.layout.header_session, null);
                            sesListView.addHeaderView(headerView);
                        }
                        sesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                                new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert)
                                        .setTitle("Delete Record")
                                        .setMessage("Are you sure you want to delete this record")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int whichButton) {

                                                String sesID=((Session)sesListView.getItemAtPosition(position)).getSessionID();

                                                SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();
                                                Session ses =sessionDao.findSessionByID(sesID);

                                                sessionDao.delete(ses);


                                                adapterSession.remove(ses);
                                                adapterSession.notifyDataSetChanged();
                                                //sesListView.
                                                //students.remove(stu);
                                                sesListView.invalidate();

                                            }})
                                        .setNegativeButton(android.R.string.no, null).show();
                            }
                        });


                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


    }

    public void AddSession(View view)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert);

        final EditText edt_SessionID = new EditText(context);
        edt_SessionID.setHint("Session ID");
        final EditText edt_ModuleCode = new EditText(context);
        edt_ModuleCode.setHint("ModuleCode");
        final EditText edt_StartTime = new EditText(context);
        edt_StartTime.setHint("StartTime");
        final EditText edt_EndTime = new EditText(context);
        edt_EndTime.setHint("EndTime");
        final EditText edt_Date = new EditText(context);
        edt_Date.setHint("Date");
        final EditText edt_Venue = new EditText(context);
        edt_Venue.setHint("Venue");
        final CheckBox chk_Recurring = new CheckBox(context);
        chk_Recurring.setHint("Recurring");
        final CheckBox chk_Compulsory = new CheckBox(context);
        chk_Compulsory.setHint("Compulsory");

        //alert.setMessage("Enter Your Message");
        alert.setTitle("Enter Session");

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.addView(edt_SessionID);
        //layout.addView(edt_ModuleCode);
        layout.addView(edt_StartTime);
        layout.addView(edt_EndTime);
        layout.addView(edt_Date);
        layout.addView(edt_Venue);
        layout.addView(chk_Recurring);
        layout.addView(chk_Compulsory);

        alert.setView(layout);



        alert.setPositiveButton("Create Session", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {



                ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();
                SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();

                final Spinner spin=(Spinner) findViewById(R.id.spn_selectModule);

                Session ses=new Session(edt_SessionID.getText().toString(),((Module)spin.getSelectedItem()).getModuleCode(),
                        edt_StartTime.getText().toString(),edt_EndTime.getText().toString(),edt_Date.getText().toString()
                        ,edt_Venue.getText().toString(),chk_Recurring.isChecked(),Boolean.toString(chk_Compulsory.isChecked()));

                Module module=(Module) spin.getSelectedItem();


                sessionDao.insert(ses);

                adapterSession.add(ses);
                adapterSession.notifyDataSetChanged();
                //modListView.refreshDrawableState();
                //modListView.invalidate();

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }


}
