package com.nmu.yearproject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Module {

    @PrimaryKey@NonNull
    private String ModuleCode;
    private String ModuleName;
    private String ModuleDescription;
    private String ModuleFaculty;

    public Module(String ModuleCode, String ModuleName, String ModuleDescription, String ModuleFaculty) {
        this.ModuleCode = ModuleCode;
        this.ModuleName = ModuleName;
        this.ModuleDescription = ModuleDescription;
        this.ModuleFaculty = ModuleFaculty;
    }

    public String getModuleCode() {
        return ModuleCode;
    }

    public void setModuleCode(String moduleCode) {
        ModuleCode = moduleCode;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String moduleName) {
        ModuleName = moduleName;
    }

    public String getModuleDescription() {
        return ModuleDescription;
    }

    public void setModuleDescription(String moduleDescription) {
        ModuleDescription = moduleDescription;
    }

    public String getModuleFaculty() {
        return ModuleFaculty;
    }

    public void setModuleFaculty(String moduleFaculty) {
        ModuleFaculty = moduleFaculty;
    }
}
