package com.nmu.yearproject;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ModuleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Module... modules);

    @Update
    public void update(Module... modules);

    @Delete
    public void delete(Module... modules);

    @Query("SELECT * FROM module")
    public Module[] loadAllModules();

    @Query("DELETE FROM Module")
    void deleteAll();

    @Query("SELECT * FROM module ORDER BY ModuleCode ASC")
    LiveData<List<Module>> getAll();

    @Query("SELECT * FROM module")
    List<Module> getAllStatic();

    @Query("SELECT * FROM module WHERE ModuleCode = :moduleCode LIMIT 1")
    Module findModuleByCode(String moduleCode);


}
