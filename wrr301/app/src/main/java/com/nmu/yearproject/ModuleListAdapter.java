package com.nmu.yearproject;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import com.nmu.yearproject.R;
import com.nmu.yearproject.appDatabase.*;
import static com.nmu.yearproject.ModuleSaveDialogFragment.TAG_DIALOG_MODULE_SAVE;

public class ModuleListAdapter extends RecyclerView.Adapter<ModuleListAdapter.ModuleViewHolder> {
    private LayoutInflater layoutInflater;
    private List<Module> moduleList;
    private Context context;

    public ModuleListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setModuleList(List<Module> moduleList) {
        this.moduleList = moduleList;
        notifyDataSetChanged();
    }

    @Override
    public ModuleListAdapter.ModuleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = layoutInflater.inflate(R.layout.item_list_module, parent, false);
        return new ModuleListAdapter.ModuleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ModuleListAdapter.ModuleViewHolder holder, int position) {
        if (moduleList == null) {
            return;
        }

        final Module module = moduleList.get(position);
        if (module != null) {
            holder.moduleText.setText(module.getModuleCode());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment dialogFragment = ModuleSaveDialogFragment.newInstance(module.getModuleCode(),module.getModuleName(),module.getModuleDescription(),module.getModuleFaculty());
                    dialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), TAG_DIALOG_MODULE_SAVE);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (moduleList == null) {
            return 0;
        } else {
            return moduleList.size();
        }
    }

    static class ModuleViewHolder extends RecyclerView.ViewHolder {
        private TextView moduleText;

        public ModuleViewHolder(View itemView) {
            super(itemView);
            moduleText = itemView.findViewById(R.id.moduleItem);
        }
    }
}
