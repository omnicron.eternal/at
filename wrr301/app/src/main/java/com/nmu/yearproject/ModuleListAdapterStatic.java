package com.nmu.yearproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ModuleListAdapterStatic extends ArrayAdapter<Module> {
    private Context context;
    int res;

    public ModuleListAdapterStatic(@NonNull Context context, int resource, @NonNull List<Module> objects) {
        super(context, resource, objects);
        this.context=context;
        res=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String ModuleCode=getItem(position).getModuleCode();
        String ModuleDescription=getItem(position).getModuleDescription();
        String ModuleFaculty=getItem(position).getModuleFaculty();
        String ModuleName=getItem(position).getModuleName();


        Module module=new Module(ModuleCode,ModuleName,ModuleDescription,ModuleFaculty);
        LayoutInflater inflater =LayoutInflater.from(context);
        convertView =inflater.inflate(res,parent,false);

        TextView ModuleCodeText= (TextView) convertView.findViewById(R.id.ModuleCodeAdapter);
        TextView ModuleNameText= (TextView) convertView.findViewById(R.id.ModuleNameAdapter);


        ModuleCodeText.setText(ModuleCode);
        ModuleNameText.setText(ModuleName);


        return convertView;
    }
}
