package com.nmu.yearproject;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class ModuleListFragment extends Fragment {
    private ModuleListAdapter moduleListAdapter;
    private ModuleViewModel moduleViewModel;
    private Context context;

    public static ModuleListFragment newInstance() {
        return new ModuleListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        moduleListAdapter = new ModuleListAdapter(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_module, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_module);
        recyclerView.setAdapter(moduleListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        return view;
    }

    private void initData() {
        moduleViewModel = ViewModelProviders.of(this).get(ModuleViewModel.class);
        moduleViewModel.getModuleList().observe(this, new Observer<List<Module>>() {
            @Override
            public void onChanged(@Nullable List<Module> modules) {
                moduleListAdapter.setModuleList(modules);
            }
        });
    }

    public void removeData() {
        if (moduleViewModel != null) {
            moduleViewModel.deleteAll();
        }
    }
}
