package com.nmu.yearproject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class ModuleSaveDialogFragment extends DialogFragment {
    private Context context;
    private String moduleCodeExtra;
    private String moduleNameExtra;
    private String moduleDescriptionExtra;
    private String moduleFacultyExtra;


    private static final String EXTRA_MODLE_CODE = "module_code";
    private static final String EXTRA_MODLE_NAME = "module_name";
    private static final String EXTRA_MODLE_DESCRIPTION = "module_description";
    private static final String EXTRA_MODLE_FACULTY = "module_faculty";

    public static final String TAG_DIALOG_MODULE_SAVE = "dialog_module_save";

    public static ModuleSaveDialogFragment newInstance(String moduleCode,String moduleName,String moduleDescription,String moduleFaculty) {
        ModuleSaveDialogFragment fragment = new ModuleSaveDialogFragment();

        Bundle args = new Bundle();
        args.putString(EXTRA_MODLE_CODE, moduleCode);
        args.putString(EXTRA_MODLE_NAME, moduleName);
        args.putString(EXTRA_MODLE_DESCRIPTION, moduleDescription);
        args.putString(EXTRA_MODLE_FACULTY, moduleFaculty);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        moduleCodeExtra = args.getString(EXTRA_MODLE_CODE);
        moduleNameExtra = args.getString(EXTRA_MODLE_NAME);
        moduleDescriptionExtra = args.getString(EXTRA_MODLE_DESCRIPTION);
        moduleFacultyExtra = args.getString(EXTRA_MODLE_FACULTY);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_module, null);
        final EditText edt_ModuleCode = view.findViewById(R.id.edt_moduleCode);
        final EditText edt_ModuleName = view.findViewById(R.id.edt_moduleName);
        final EditText edt_ModuleDescription = view.findViewById(R.id.edt_moduleDescription);
        final EditText edt_ModuleFaculty = view.findViewById(R.id.edt_moduleFaculty);

        if (moduleCodeExtra != null) {
            edt_ModuleCode.setText(moduleCodeExtra);
            edt_ModuleCode.setSelection(moduleCodeExtra.length());
        }
        if (moduleNameExtra != null) {
            edt_ModuleName.setText(moduleNameExtra);
            edt_ModuleName.setSelection(moduleNameExtra.length());
        }
        if (moduleDescriptionExtra != null) {
            edt_ModuleDescription.setText(moduleDescriptionExtra);
            edt_ModuleDescription.setSelection(moduleDescriptionExtra.length());
        }
        if (moduleFacultyExtra != null) {
            edt_ModuleFaculty.setText(moduleFacultyExtra);
            edt_ModuleFaculty.setSelection(moduleFacultyExtra.length());
        }


        alertDialogBuilder.setView(view)
                .setTitle("Enter new module")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveModule(edt_ModuleCode.getText().toString(),edt_ModuleName.getText().toString(),edt_ModuleDescription.getText().toString(),edt_ModuleFaculty.getText().toString());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteModule(edt_ModuleCode.getText().toString());
                    }
                });

        return alertDialogBuilder.create();
    }

    private void saveModule(String moduleCode,String moduleName,String moduleDescription,String moduleFaculty) {
        if (TextUtils.isEmpty(moduleCode)) {
            return;
        }

        ModuleDao moduleDao = appDatabase.getDatabase(context).moduleDao();

        if (moduleCodeExtra != null) {
            // clicked on item row -> update
            Module moduleToUpdate = moduleDao.findModuleByCode(moduleCodeExtra);
            if (moduleToUpdate != null) {
                if (!moduleToUpdate.getModuleCode().equals(moduleCode)) {
                    moduleToUpdate.setModuleCode(moduleCode);
                    moduleDao.update(moduleToUpdate);
                }
            }
        } else {
            moduleDao.insert(new Module(moduleCode,moduleName,moduleDescription,moduleFaculty));
        }
    }

    private void deleteModule(String moduleCode)
    {
        ModuleDao moduleDao = appDatabase.getDatabase(context).moduleDao();
        if (moduleCodeExtra != null) {
            Module moduleToDelete =moduleDao.findModuleByCode(moduleCode);
            moduleDao.delete(moduleToDelete);
        }


    }
}
