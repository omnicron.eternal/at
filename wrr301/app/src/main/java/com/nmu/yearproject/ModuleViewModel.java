package com.nmu.yearproject;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class ModuleViewModel extends AndroidViewModel {
    private ModuleDao moduleDao;
    private LiveData<List<Module>> moduleLiveData;

    public ModuleViewModel(@NonNull Application application) {
        super(application);
        moduleDao = appDatabase.getDatabase(application).moduleDao();
        moduleLiveData = moduleDao.getAll();
    }

    public LiveData<List<Module>> getModuleList() {
        return moduleLiveData;
    }

    public void insert(Module... modules) {
        moduleDao.insert(modules);
    }

    public void update(Module module) {
        moduleDao.update(module);
    }

    public void deleteAll() {
        moduleDao.deleteAll();
    }
}
