package com.nmu.yearproject;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class Register extends AppCompatActivity {
    private Context context;

    //public abstract StudentDao studentDao();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


    }
    public void register(View view) {
        EditText sNum = (EditText) findViewById(R.id.edt_sNum);
        EditText name = (EditText) findViewById(R.id.edt_name);
        EditText surname = (EditText) findViewById(R.id.edt_surname);
        EditText email = (EditText) findViewById(R.id.edt_email);
        EditText pass= (EditText) findViewById(R.id.edt_password);
        CheckBox lect=(CheckBox) findViewById(R.id.chk_lect);
        CheckBox assist=(CheckBox) findViewById(R.id.chk_assist);

        Student stu=new Student(sNum.getText().toString(),name.getText().toString(),surname.getText().toString(),email.getText().toString(),pass.toString(),lect.isChecked(),assist.isChecked());
        SaveStudent(sNum.getText().toString(),name.getText().toString(),surname.getText().toString(),email.getText().toString(),pass.getText().toString(),lect.isChecked(),assist.isChecked());

    }
    private void SaveStudent(String sNum,String name,String surname,String email,String pass,Boolean lect,Boolean assist) {
        if (TextUtils.isEmpty(sNum)) {
            return;
        }

        StudentDao studentDao = appDatabase.getDatabase(getApplicationContext()).studentDao();

        if (sNum != null) {
            // clicked on item row -> update
            Student studentToUpdate = studentDao.findStudentByStudentNum(sNum);
            Student insertStu=new Student(sNum,name,surname,email,pass,lect,assist);
            if (studentToUpdate != null) {
                if (studentToUpdate.getStudentNumber().equals(sNum)) {

                    //studentToUpdate.setStudentNumber(sNum);
                    //studentDao.update(studentToUpdate);
                    studentDao.insert(insertStu);
                }
            }
            else
            {
                studentDao.insert(new Student(sNum,name,surname,email,pass,lect,assist));
                Student stu =studentDao.findStudentByStudentNum(sNum);
                if (stu.getLecturer())
                {
                    Intent intent = new Intent(this, LecturerHome.class);
                    intent.putExtra("EXTRA_STUDENT",stu);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(this, StudentHome.class);
                    intent.putExtra("EXTRA_STUDENT",stu);
                    startActivity(intent);
                }
            }
        }

    }

}
