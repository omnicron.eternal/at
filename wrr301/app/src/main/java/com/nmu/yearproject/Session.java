package com.nmu.yearproject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Module.class,
        parentColumns = "ModuleCode",childColumns ="ModuleCode",onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE))
public class Session {
    @PrimaryKey@NonNull
    private String SessionID;
    private String ModuleCode;
    private String StartTime;
    private String EndTime;
    private String Date;
    private String Venue;
    private Boolean Recurring;
    private String Compulsory;

    public Session(String SessionID, String ModuleCode, String StartTime, String EndTime, String Date, String Venue, Boolean Recurring, String Compulsory) {
        this.SessionID = SessionID;
        this.ModuleCode = ModuleCode;
        this.StartTime = StartTime;
        this.EndTime = EndTime;
        this.Date = Date;
        this.Venue = Venue;
        this.Recurring = Recurring;
        this.Compulsory = Compulsory;
    }

    public String getSessionID() {
        return SessionID;
    }

    public void setSessionID(String sessionID) {
        SessionID = sessionID;
    }

    public String getModuleCode() {
        return ModuleCode;
    }

    public void setModuleCode(String moduleCode) {
        ModuleCode = moduleCode;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getVenue() {
        return Venue;
    }

    public void setVenue(String venue) {
        Venue = venue;
    }

    public Boolean getRecurring() {
        return Recurring;
    }

    public void setRecurring(Boolean recurring) {
        Recurring = recurring;
    }

    public String getCompulsory() {
        return Compulsory;
    }

    public void setCompulsory(String compulsory) {
        Compulsory = compulsory;
    }
}
