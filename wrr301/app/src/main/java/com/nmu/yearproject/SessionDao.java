package com.nmu.yearproject;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SessionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Session... sessions);

    @Update
    public void update(Session... sessions);

    @Delete
    public void delete(Session... sessions);

    @Query("SELECT * FROM session")
    public Session[] loadAllSessions();

    @Query("DELETE FROM Session")
    void deleteAll();

    @Query("SELECT * FROM session ORDER BY SessionID ASC")
    LiveData<List<Session>> getAll();

    @Query("SELECT * FROM session")
    List<Session> getAllStatic();


    @Query("SELECT * FROM session WHERE SessionID = :sessionID LIMIT 1")
    Session findSessionByID(String sessionID);

    @Query("SELECT * FROM session WHERE ModuleCode = :modCode")
    List<Session> findSessionByModuleCode(String modCode);
}
