package com.nmu.yearproject;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static com.nmu.yearproject.SessionSaveDialogFragment.TAG_DIALOG_SESSION_SAVE;

public class SessionListAdapter extends RecyclerView.Adapter<SessionListAdapter.SessionViewHolder> {
    private LayoutInflater layoutInflater;
    private List<Session> sessionList;
    private Context context;

    public SessionListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setSessionList(List<Session> sessionList) {
        this.sessionList = sessionList;
        notifyDataSetChanged();
    }

    @Override
    public SessionListAdapter.SessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = layoutInflater.inflate(R.layout.item_list_session, parent, false);
        return new SessionListAdapter.SessionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SessionListAdapter.SessionViewHolder holder, int position) {
        if (sessionList == null) {
            return;
        }

        final Session session = sessionList.get(position);
        if (session != null) {
            holder.sessionText.setText(session.getSessionID());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment dialogFragment = SessionSaveDialogFragment.newInstance(session.getSessionID(), session.getModuleCode(), session.getStartTime(), session.getEndTime(), session.getDate(), session.getVenue(), session.getRecurring().toString(), session.getCompulsory());
                    dialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), TAG_DIALOG_SESSION_SAVE);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (sessionList == null) {
            return 0;
        } else {
            return sessionList.size();
        }
    }

    static class SessionViewHolder extends RecyclerView.ViewHolder {
        private TextView sessionText;

        public SessionViewHolder(View itemView) {
            super(itemView);
            sessionText = itemView.findViewById(R.id.sessionItem);
        }
    }
}
