package com.nmu.yearproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class SessionListAdapterStatic extends ArrayAdapter<Session> {
    private Context context;
    int res;

    public SessionListAdapterStatic(@NonNull Context context, int resource, @NonNull List<Session> objects) {
        super(context, resource, objects);
        this.context=context;
        res=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String SessionID=getItem(position).getSessionID();
        String ModuleCode=getItem(position).getModuleCode();
        String StartTime=getItem(position).getStartTime();
        String EndTime=getItem(position).getEndTime();
        String Date=getItem(position).getDate();
        String Venue=getItem(position).getVenue();
        Boolean Recurring=getItem(position).getRecurring();
        String Compulsory=getItem(position).getCompulsory();

        Session ses=new Session(SessionID,ModuleCode,StartTime,EndTime,Date,Venue,Recurring,Compulsory);
        LayoutInflater inflater =LayoutInflater.from(context);
        convertView =inflater.inflate(res,parent,false);

        TextView ModuleText= (TextView) convertView.findViewById(R.id.ModuleCode);
        TextView StartTimeText= (TextView) convertView.findViewById(R.id.StartTime);
        TextView EndTimeText= (TextView) convertView.findViewById(R.id.EndTime);
        TextView DateText= (TextView) convertView.findViewById(R.id.Date);
        TextView VenueText= (TextView) convertView.findViewById(R.id.Venue);
        TextView RecurringText= (TextView) convertView.findViewById(R.id.Recurring);
        TextView CompulsoryText= (TextView) convertView.findViewById(R.id.Compulsory);

        ModuleText.setText(ModuleCode);
        StartTimeText.setText(StartTime);
        EndTimeText.setText(EndTime);
        DateText.setText(Date);
        VenueText.setText(Venue);
        String RecurringString="";
        if (Recurring) RecurringString="YES";
        else RecurringString="NO";
        RecurringText.setText(RecurringString);
        CompulsoryText.setText(Compulsory);

        return convertView;
    }
}
