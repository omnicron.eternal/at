package com.nmu.yearproject;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class SessionListFragment extends Fragment {
    private SessionListAdapter sessionListAdapter;
    private SessionViewModel sessionViewModel;
    private Context context;

    public static SessionListFragment newInstance() {
        return new SessionListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        sessionListAdapter = new SessionListAdapter(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_session, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_session);
        recyclerView.setAdapter(sessionListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        return view;
    }

    private void initData() {
        sessionViewModel = ViewModelProviders.of(this).get(SessionViewModel.class);
        sessionViewModel.getSessionList().observe(this, new Observer<List<Session>>() {
            @Override
            public void onChanged(@Nullable List<Session> sessions) {
                sessionListAdapter.setSessionList(sessions);
            }
        });
    }

    public void removeData() {
        if (sessionViewModel != null) {
            sessionViewModel.deleteAll();
        }
    }
}
