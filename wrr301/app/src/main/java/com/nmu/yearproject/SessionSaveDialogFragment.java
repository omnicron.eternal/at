package com.nmu.yearproject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class SessionSaveDialogFragment extends DialogFragment {

    private Context context;
    private String SessionIDExtra;
    private String ModuleCodeExtra;
    private String StartTimeExtra;
    private String EndTimeExtra;
    private String DateExtra;
    private String VenueExtra;
    private Boolean RecurringExtra;
    private String CompulsoryExtra;


    private static final String EXTRA_MODLE_CODE = "module_code";
    private static final String EXTRA_SESSION_ID = "session_id";
    private static final String EXTRA_START_TIME = "start_time";
    private static final String EXTRA_END_TIME= "end_time";
    private static final String EXTRA_DATE = "date";
    private static final String EXTRA_VENUE = "venue";
    private static final String EXTRA_RECURRING= "recurring";
    private static final String EXTRA_COMPULSORY= "compulsory";




    public static final String TAG_DIALOG_SESSION_SAVE = "dialog_session_save";

    public static SessionSaveDialogFragment newInstance(String sessionID,String moduleCode,String startTime,String endTime,String date,String venue,String recurring,String compulsory) {
        SessionSaveDialogFragment fragment = new SessionSaveDialogFragment();

        Bundle args = new Bundle();
        args.putString(EXTRA_MODLE_CODE, moduleCode);
        args.putString(EXTRA_SESSION_ID, sessionID);
        args.putString(EXTRA_START_TIME, startTime);
        args.putString(EXTRA_END_TIME, endTime);
        args.putString(EXTRA_DATE, date);
        args.putString(EXTRA_VENUE, venue);
        args.putString(EXTRA_RECURRING, recurring);
        args.putString(EXTRA_COMPULSORY, compulsory);


        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        ModuleCodeExtra = args.getString(EXTRA_MODLE_CODE);
        SessionIDExtra = args.getString(EXTRA_SESSION_ID);
        StartTimeExtra = args.getString(EXTRA_START_TIME);
        EndTimeExtra = args.getString(EXTRA_END_TIME);
        DateExtra = args.getString(EXTRA_DATE);
        VenueExtra = args.getString(EXTRA_VENUE);
        RecurringExtra = Boolean.parseBoolean(args.getString(EXTRA_RECURRING));
        CompulsoryExtra = args.getString(EXTRA_COMPULSORY);


    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_session, null);

        final EditText edt_ModuleCode = view.findViewById(R.id.edt_moduleCode);
        final EditText edt_SessionID = view.findViewById(R.id.edt_sessionID);
        final EditText edt_StartTime = view.findViewById(R.id.edt_startTime);
        final EditText edt_EndTime = view.findViewById(R.id.edt_endTime);
        final EditText edt_Date = view.findViewById(R.id.edt_date);
        final EditText edt_Venue = view.findViewById(R.id.edt_venue);
        final EditText edt_Recurring = view.findViewById(R.id.edt_recurring);
        final EditText edt_Compulsory = view.findViewById(R.id.edt_compulsory);

        if (ModuleCodeExtra != null) {
            edt_ModuleCode.setText(ModuleCodeExtra);
            edt_ModuleCode.setSelection(ModuleCodeExtra.length());
        }
        if (SessionIDExtra != null) {
            edt_SessionID.setText(SessionIDExtra);
            edt_SessionID.setSelection(SessionIDExtra.length());
        }
        if (StartTimeExtra != null) {
            edt_StartTime.setText(StartTimeExtra);
            edt_StartTime.setSelection(StartTimeExtra.length());
        }
        if (EndTimeExtra != null) {
            edt_EndTime.setText(EndTimeExtra);
            edt_EndTime.setSelection(EndTimeExtra.length());
        }
        if (DateExtra != null) {
            edt_Date.setText(DateExtra);
            edt_Date.setSelection(DateExtra.length());
        }
        if (VenueExtra != null) {
            edt_Venue.setText(VenueExtra);
            edt_Venue.setSelection(VenueExtra.length());
        }
        if (RecurringExtra != null) {
            edt_Recurring.setText(RecurringExtra.toString());
            edt_Recurring.setSelection(RecurringExtra.toString().length());
        }
        if (CompulsoryExtra != null) {
            edt_Compulsory.setText(CompulsoryExtra);
            edt_Compulsory.setSelection(CompulsoryExtra.length());
        }


        alertDialogBuilder.setView(view)
                .setTitle("Enter new Session")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveSession(edt_SessionID.getText().toString(),edt_ModuleCode.getText().toString(),edt_StartTime.getText().toString(),edt_EndTime.getText().toString(),edt_Date.getText().toString(),edt_Venue.getText().toString(),edt_Recurring.getText().toString(),edt_Compulsory.getText().toString());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSession(edt_SessionID.getText().toString());
                    }
                });

        return alertDialogBuilder.create();
    }

    private void saveSession(String sessionID,String moduleCode,String startTime,String endTime,String date,String venue,String recurring,String compulsory) {
        if (TextUtils.isEmpty(sessionID)) {
            return;
        }

        SessionDao sessionDao = appDatabase.getDatabase(context).sessionDao();

        if (SessionIDExtra != null) {
            // clicked on item row -> update
            Session sessionToUpdate = sessionDao.findSessionByID(sessionID);
            if (sessionToUpdate != null) {
                if (!sessionToUpdate.getSessionID().equals(sessionID)) {
                    sessionToUpdate.setSessionID(sessionID);
                    sessionDao.update(sessionToUpdate);
                }
            }
        } else {
            sessionDao.insert(new Session(sessionID,moduleCode,startTime,endTime,date,venue,Boolean.parseBoolean(recurring),compulsory));
        }
    }

    private void deleteSession(String sessionID)
    {
        SessionDao sessionDao = appDatabase.getDatabase(context).sessionDao();
        if (SessionIDExtra != null) {
            Session sessionToDelete =sessionDao.findSessionByID(sessionID);
            sessionDao.delete(sessionToDelete);
        }


    }
}
