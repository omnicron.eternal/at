package com.nmu.yearproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

public class SessionTrendAdapter extends ArrayAdapter<Session> {
    private Context context;
    int res;

    public SessionTrendAdapter(@NonNull Context context, int resource, @NonNull List<Session> objects) {
        super(context, resource, objects);
        this.context=context;
        res=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String SessionID=getItem(position).getSessionID();
        String ModuleCode=getItem(position).getModuleCode();
        String StartTime=getItem(position).getStartTime();
        String EndTime=getItem(position).getEndTime();
        String Date=getItem(position).getDate();
        String Venue=getItem(position).getVenue();
        Boolean Recurring=getItem(position).getRecurring();
        String Compulsory=getItem(position).getCompulsory();

        Session ses=new Session(SessionID,ModuleCode,StartTime,EndTime,Date,Venue,Recurring,Compulsory);
        LayoutInflater inflater =LayoutInflater.from(context);
        convertView =inflater.inflate(res,parent,false);

        TextView ModuleText= (TextView) convertView.findViewById(R.id.ModuleCodeTrend);
        TextView StartTimeText= (TextView) convertView.findViewById(R.id.StartTimeTrend);
        TextView DateText= (TextView) convertView.findViewById(R.id.DateTrend);
        TextView VenueText= (TextView) convertView.findViewById(R.id.VenueTrend);
        TextView AttendanceNumber= (TextView) convertView.findViewById(R.id.AttendanceNumberTrend);

        StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(context).studentSessionJoinDao();
        List<Student> attendance=studentSessionJoinDao.getStudentForSessionStatic(ses.getSessionID());

        ModuleText.setText(ModuleCode);
        StartTimeText.setText(StartTime);
        DateText.setText(Date);
        VenueText.setText(Venue);
        AttendanceNumber.setText(Integer.toString(attendance.size()));

        return convertView;
    }
}
