package com.nmu.yearproject;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class SessionViewModel extends AndroidViewModel {
    private SessionDao sessionDao;
    private LiveData<List<Session>> sessionLiveData;

    public SessionViewModel(@NonNull Application application) {
        super(application);
        sessionDao = appDatabase.getDatabase(application).sessionDao();
        sessionLiveData = sessionDao.getAll();
    }

    public LiveData<List<Session>> getSessionList() {
        return sessionLiveData;
    }

    public void insert(Session... sessions) {
        sessionDao.insert(sessions);
    }

    public void update(Session session) {
        sessionDao.update(session);
    }

    public void deleteAll() {
        sessionDao.deleteAll();
    }
}
