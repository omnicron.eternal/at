package com.nmu.yearproject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity
public class Student implements Serializable {
    @PrimaryKey@NonNull
    private String StudentNumber;
    private String Name;
    private String Surname;
    private String Email;
    private String Password;
    private Boolean Lecturer;
    private Boolean Assistant;

    public Student(String StudentNumber, String Name, String Surname, String Email,String Password,Boolean Lecturer,Boolean Assistant) {
        this.StudentNumber = StudentNumber;
        this.Name = Name;
        this.Surname = Surname;
        this.Email = Email;
        this.Password=Password;
        this.Lecturer=Lecturer;
        this.Assistant=Assistant;
    }

    public String getStudentNumber() {
        return StudentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        StudentNumber = studentNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public Boolean getLecturer() {
        return Lecturer;
    }

    public void setLecturer(Boolean lecturer) {
        Lecturer = lecturer;
    }

    public Boolean getAssistant() {
        return Assistant;
    }

    public void setAssistant(Boolean assistant) {
        Assistant = assistant;
    }
}
