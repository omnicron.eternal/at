package com.nmu.yearproject;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface StudentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Student... students);

    @Update
    public void update(Student... students);

    @Delete
    public void delete(Student... students);

    @Query("DELETE FROM Student")
    void deleteAll();

    @Query("SELECT * FROM student ORDER BY surname ASC")
    LiveData<List<Student>> getAll();

    @Query("SELECT * FROM student WHERE StudentNumber = :sNum LIMIT 1")
    Student findStudentByStudentNum(String sNum);

    }
