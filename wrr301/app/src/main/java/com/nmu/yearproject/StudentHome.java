package com.nmu.yearproject;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class StudentHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home_matt);
        Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");
        Button btnAttendance=(Button) findViewById(R.id.button13);
        if (!stu.getAssistant()) btnAttendance.setVisibility(View.GONE);

        
        
        ModuleDao moduleDao = appDatabase.getDatabase(getBaseContext()).moduleDao();
        StudentModuleJoinDao studentModuleJoinDao = appDatabase.getDatabase(getBaseContext()).studentModuleJoinDao();
        SessionDao sessionDao = appDatabase.getDatabase(getBaseContext()).sessionDao();
        
        List<Module> moduleList = studentModuleJoinDao.getModuleForStudentStatic(stu.getStudentNumber());

        List<Session> sessionList = new ArrayList<Session>();
        for (Module module : moduleList) {
            List<Session> tempSesList = sessionDao.findSessionByModuleCode(module.getModuleCode());
            sessionList.addAll(tempSesList);
        }
        if (!sessionList.isEmpty()) {
            List<Date> dates = new ArrayList<Date>();
            for (Session session : sessionList) {
                String tempDateString = session.getDate();
                try {
                    Date date1 = new SimpleDateFormat("yyyy/MM/dd").parse(tempDateString);
                    Calendar calendar=Calendar.getInstance();
                    calendar.setTime(date1);
                    calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(session.getStartTime().substring(0,2)));
                    calendar.set(Calendar.MINUTE, Integer.parseInt(session.getStartTime().substring(3,5)));
                    Date date=calendar.getTime();

                    dates.add(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            final long now = System.currentTimeMillis();
            Date currentTime = Calendar.getInstance().getTime();
            List<Date> datesAfter = new ArrayList<Date>();


            for (Date date : dates) {
                if (date.after(currentTime))datesAfter.add(date);
            }

            if (datesAfter.isEmpty())
            {
                showNotification("Next Session", "You have no upcoming sessions");
            }
            else {
                Date closest = Collections.min(datesAfter, new Comparator<Date>() {
                    public int compare(Date d1, Date d2) {
                        long diff1 = Math.abs(d1.getTime() - now);
                        long diff2 = Math.abs(d2.getTime() - now);
                        return Long.compare(diff1, diff2);
                    }
                });

                showNotification("Next Session", closest.toString());
            }
        }
        else showNotification("Next Session", "You have no upcoming sessions");


    }

    public void maintainSession(View view) {
        Intent intent = new Intent(this, MaintainSession.class);
        Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");
        intent.putExtra("EXTRA_STUDENT",stu);
        startActivity(intent);
    }
    public void takeAttendance(View view) {
        Intent intent = new Intent(this, TakeAttendance.class);
        startActivity(intent);
    }

    public void viewTimetable(View view) {
        Intent intent = new Intent(this, ViewTimetableActivity.class);
        Student stu = (Student) getIntent().getSerializableExtra("EXTRA_STUDENT");
        intent.putExtra("EXTRA_STUDENT", stu);
        startActivity(intent);
    }
    void showNotification(String title, String content) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "YOUR_CHANNEL_NAME", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DISCRIPTION");
            mNotificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(content)// message for notification
                //.setSound(alarmSound) // set alarm sound for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        mNotificationManager.notify(0, mBuilder.build());
    }
}
