package com.nmu.yearproject;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static com.nmu.yearproject.StudentSaveDialogFragment.TAG_DIALOG_STUDENT_SAVE;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.StudentViewHolder> {
    private LayoutInflater layoutInflater;
    private List<Student> studentList;
    private Context context;

    public StudentListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
        notifyDataSetChanged();
    }

    @Override
    public StudentListAdapter.StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = layoutInflater.inflate(R.layout.item_list_student, parent, false);
        return new StudentListAdapter.StudentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StudentListAdapter.StudentViewHolder holder, int position) {
        if (studentList == null) {
            return;
        }

        final Student student = studentList.get(position);
        if (student != null) {
            holder.studentText.setText(student.getStudentNumber());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment dialogFragment = StudentSaveDialogFragment.newInstance(student.getStudentNumber(),student.getName(),student.getSurname(),student.getEmail(),student.getPassword(),student.getLecturer(),student.getAssistant());
                    dialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), TAG_DIALOG_STUDENT_SAVE);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (studentList == null) {
            return 0;
        } else {
            return studentList.size();
        }
    }

    static class StudentViewHolder extends RecyclerView.ViewHolder {
        private TextView studentText;

        public StudentViewHolder(View itemView) {
            super(itemView);
            studentText = itemView.findViewById(R.id.studentItem);
        }
    }
}
