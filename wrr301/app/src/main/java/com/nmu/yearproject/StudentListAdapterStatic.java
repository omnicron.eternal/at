package com.nmu.yearproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class StudentListAdapterStatic extends ArrayAdapter<Student> {
    private Context context;
    int res;

    public StudentListAdapterStatic(@NonNull Context context, int resource, @NonNull List<Student> objects) {
        super(context, resource, objects);
        this.context=context;
        res=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String StudentNum=getItem(position).getStudentNumber();
        String Surname=getItem(position).getSurname();


        LayoutInflater inflater =LayoutInflater.from(context);
        convertView =inflater.inflate(res,parent,false);

        TextView StudentNumText= (TextView) convertView.findViewById(R.id.StudentNumberAdapter);
        TextView SurnameText= (TextView) convertView.findViewById(R.id.StudentSurnameAdapter);


        StudentNumText.setText(StudentNum);
        SurnameText.setText(Surname);


        return convertView;
    }
}
