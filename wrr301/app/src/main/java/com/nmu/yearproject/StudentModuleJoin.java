package com.nmu.yearproject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

@Entity(tableName = "student_module_join",
        primaryKeys = { "sNumber", "modCode" },
        foreignKeys = {
                @ForeignKey(entity = Student.class,
                        parentColumns = "StudentNumber",
                        childColumns = "sNumber",
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = Module.class,
                        parentColumns = "ModuleCode",
                        childColumns = "modCode",
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE)
        })
public class StudentModuleJoin {
    @NonNull
    public final String sNumber;
    @NonNull
    public final String modCode;

    public StudentModuleJoin(final String sNumber, final String modCode)
    {
        this.sNumber = sNumber;
        this.modCode = modCode;
    }

}
