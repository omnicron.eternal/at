package com.nmu.yearproject;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface StudentModuleJoinDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(StudentModuleJoin... studentModuleJoin);

    @Update
    public void update(StudentModuleJoin... studentModuleJoin);

    @Query("DELETE FROM student_module_join")
    void deleteAll();

    @Delete
    public void delete(StudentModuleJoin... studentModuleJoin);

    @Query("SELECT * FROM Student INNER JOIN student_module_join ON student.StudentNumber =student_module_join.sNumber WHERE student_module_join.modCode=:moduleCode")
    LiveData<List<Student>> getStudentForModule(final String moduleCode);

    @Query("SELECT * FROM Student INNER JOIN student_module_join ON student.StudentNumber =student_module_join.sNumber WHERE student_module_join.modCode=:moduleCode")
    List<Student> getStudentForModuleStatic(final String moduleCode);

    @Query("SELECT * FROM Module INNER JOIN student_module_join ON Module.ModuleCode=student_module_join.modCode WHERE student_module_join.sNumber=:sNum")
    LiveData<List<Module>> getModuleForStudent(final String sNum);

    @Query("SELECT * FROM Module INNER JOIN student_module_join ON Module.ModuleCode=student_module_join.modCode WHERE student_module_join.sNumber=:sNum")
    List<Module> getModuleForStudentStatic(final String sNum);

    @Query("SELECT * FROM student_module_join ")
    LiveData<List<StudentModuleJoin>> getAll();

    @Query("SELECT * FROM student_module_join WHERE sNumber = :sNum AND modCode=:moduleCode  LIMIT 1")
    StudentModuleJoin findStudentModuleJoin(String sNum,String moduleCode);


}
