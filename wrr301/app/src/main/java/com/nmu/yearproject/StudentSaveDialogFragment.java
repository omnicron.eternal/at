package com.nmu.yearproject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class StudentSaveDialogFragment extends DialogFragment {

    private Context context;
    private String sNumExtra;
    private String nameExtra;
    private String surnameExtra;
    private String emailExtra;
    private String passExtra;
    private Boolean lectExtra;
    private Boolean assistExtra;


    private static final String EXTRA_SNUM = "snum";
    private static final String EXTRA_NAME = "name";
    private static final String EXTRA_SURNAME = "surname";
    private static final String EXTRA_EMAIL= "email";
    private static final String EXTRA_PASSWORD="pass";
    private static final String EXTRA_LECTURER="lect";
    private static final String EXTRA_ASSISTANT="assist";

    public static final String TAG_DIALOG_STUDENT_SAVE = "dialog_student_save";

    public static StudentSaveDialogFragment newInstance(String sNum,String name,String surname,String email,String pass,Boolean lect,Boolean assist) {
        StudentSaveDialogFragment fragment = new StudentSaveDialogFragment();

        Bundle args = new Bundle();
        args.putString(EXTRA_SNUM, sNum);
        args.putString(EXTRA_NAME, name);
        args.putString(EXTRA_SURNAME, surname);
        args.putString(EXTRA_EMAIL, email);
        args.putString(EXTRA_PASSWORD, pass);
        args.putBoolean(EXTRA_LECTURER,lect);
        args.putBoolean(EXTRA_ASSISTANT,assist);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        sNumExtra = args.getString(EXTRA_SNUM);
        nameExtra = args.getString(EXTRA_NAME);
        surnameExtra = args.getString(EXTRA_SURNAME);
        emailExtra = args.getString(EXTRA_EMAIL);
        passExtra=args.getString(EXTRA_PASSWORD);
        lectExtra=args.getBoolean(EXTRA_LECTURER);
        assistExtra=args.getBoolean(EXTRA_ASSISTANT);



    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_student, null);

        final EditText edt_sNum = view.findViewById(R.id.edt_sNumCapture);
        final EditText edt_name = view.findViewById(R.id.edt_nameCapture);
        final EditText edt_surname = view.findViewById(R.id.edt_surnameCapture);
        final EditText edt_email = view.findViewById(R.id.edt_emailCapture);
        final EditText edt_pass = view.findViewById(R.id.edt_passwordCapture);
        final CheckBox chk_lect=view.findViewById(R.id.chk_lecturerCapture);
        final CheckBox chk_assist=view.findViewById(R.id.chk_assistantCapture);

        if (sNumExtra != null) {
            edt_sNum.setText(sNumExtra);
            edt_sNum.setSelection(sNumExtra.length());
        }
        if (nameExtra != null) {
            edt_name.setText(nameExtra);
            edt_name.setSelection(nameExtra.length());
        }
        if (surnameExtra != null) {
            edt_surname.setText(surnameExtra);
            edt_surname.setSelection(surnameExtra.length());
        }
        if (emailExtra != null) {
            edt_email.setText(emailExtra);
            edt_email.setSelection(emailExtra.length());
        }
        if (passExtra!=null)
        {
            edt_pass.setText(passExtra);
            edt_pass.setSelection(passExtra.length());
        }
        if (lectExtra!=null)
        {
            chk_lect.setChecked(lectExtra);
        }
        if (assistExtra!=null)
        {
            chk_assist.setChecked(assistExtra);
        }


        alertDialogBuilder.setView(view)
                .setTitle("Enter new student")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveStudent(edt_sNum.getText().toString(),edt_name.getText().toString(),edt_surname.getText().toString(),edt_email.getText().toString(),edt_pass.getText().toString(),chk_lect.isChecked(),chk_assist.isChecked());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteStudent(edt_sNum.getText().toString());
                    }
                });

        return alertDialogBuilder.create();
    }

    private void saveStudent(String sNum,String name,String surname,String email,String pass,Boolean lect,Boolean assist) {
        if (TextUtils.isEmpty(sNum)) {
            return;
        }

        StudentDao studentDao = appDatabase.getDatabase(context).studentDao();

        if (sNumExtra != null) {
            // clicked on item row -> update
            Student studentToUpdate = studentDao.findStudentByStudentNum(sNum);
            if (studentToUpdate != null) {
                if (!studentToUpdate.getStudentNumber().equals(sNum)) {
                    studentToUpdate.setStudentNumber(sNum);
                    studentDao.update(studentToUpdate);
                }
            }
        }
        else
        {
            studentDao.insert(new Student(sNum,name,surname,email,pass,lect,assist));
        }
    }

    private void deleteStudent(String sNum)
    {
        StudentDao studentDao = appDatabase.getDatabase(context).studentDao();
        if (sNumExtra != null) {
            Student studentToDelete =studentDao.findStudentByStudentNum(sNum);
            studentDao.delete(studentToDelete);
        }


    }
}
