package com.nmu.yearproject;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

@Entity(tableName = "student_session_join",
        primaryKeys = { "sNum", "sesID" },
        foreignKeys = {
                @ForeignKey(entity = Student.class,
                        parentColumns = "StudentNumber",
                        childColumns = "sNum",
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = Session.class,
                        parentColumns = "SessionID",
                        childColumns = "sesID",
                        onUpdate = ForeignKey.CASCADE,
                        onDelete = ForeignKey.CASCADE)
        })

public class StudentSessionJoin {
    @NonNull
    public final String sNum;
    @NonNull
    public final String sesID;

    public StudentSessionJoin(final String sNum, final String sesID)
    {
        this.sNum = sNum;
        this.sesID = sesID;
    }

}
