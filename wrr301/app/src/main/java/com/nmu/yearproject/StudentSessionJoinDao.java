package com.nmu.yearproject;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface StudentSessionJoinDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(StudentSessionJoin... studentSessionJoin);

    @Update
    public void update(StudentSessionJoin... studentSessionJoin);

    @Query("DELETE FROM student_session_join")
        void deleteAll();

    @Delete
    public void delete(StudentSessionJoin... studentSessionJoin);

    @Query("SELECT * FROM Student INNER JOIN student_session_join ON student.StudentNumber =student_session_join.sNum WHERE student_session_join.sesID=:sesID")
            LiveData<List<Student>> getStudentForSession(final String sesID);

    @Query("SELECT * FROM Student INNER JOIN student_session_join ON student.StudentNumber =student_session_join.sNum WHERE student_session_join.sesID=:sesID")
            List<Student> getStudentForSessionStatic(final String sesID);

    @Query("SELECT * FROM Session INNER JOIN student_session_join ON Session.SessionID=student_session_join.sesID WHERE student_session_join.sNum=:sNum")
            LiveData<List<Session>> getSessionForStudent(final String sNum);

    @Query("SELECT * FROM Session INNER JOIN student_session_join ON Session.SessionID=student_session_join.sesID WHERE student_session_join.sNum=:sNum")
            List<Session> getSessionForStudentStatic(final String sNum);

    @Query("SELECT * FROM student_session_join ")
            LiveData<List<StudentSessionJoin>> getAll();

    @Query("SELECT * FROM student_session_join WHERE sNum = :sNum AND sesID=:sesID  LIMIT 1")
            StudentSessionJoin findStudentSessionJoin(String sNum,String sesID);


}
