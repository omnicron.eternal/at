package com.nmu.yearproject;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static com.nmu.yearproject.StudentSessionJoinSaveDialogFragment.TAG_DIALOG_STUDENTSESSIONJOIN_SAVE;

public class StudentSessionJoinListAdapter extends RecyclerView.Adapter<StudentSessionJoinListAdapter.StudentSessionJoinViewHolder> {
    private LayoutInflater layoutInflater;
    private List<StudentSessionJoin> studentSessionJoinList;
    private Context context;

    public StudentSessionJoinListAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setStudentSessionJoinList(List<StudentSessionJoin> studentSessionJoinList) {
        this.studentSessionJoinList = studentSessionJoinList;
        notifyDataSetChanged();
    }

    @Override
    public StudentSessionJoinListAdapter.StudentSessionJoinViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = layoutInflater.inflate(R.layout.item_list_studentsessionjoin, parent, false);
        return new StudentSessionJoinListAdapter.StudentSessionJoinViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StudentSessionJoinListAdapter.StudentSessionJoinViewHolder holder, int position) {
        if (studentSessionJoinList == null) {
            return;
        }

        final StudentSessionJoin studentSessionJoin = studentSessionJoinList.get(position);
        if (studentSessionJoin != null) {
            holder.studentSessionJoinText.setText(studentSessionJoin.sesID+" "+studentSessionJoin.sNum);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment dialogFragment = StudentSessionJoinSaveDialogFragment.newInstance(studentSessionJoin.sNum,studentSessionJoin.sesID);
                    dialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), TAG_DIALOG_STUDENTSESSIONJOIN_SAVE);
                }
            })

            ;
        }
    }

    @Override
    public int getItemCount() {
        if (studentSessionJoinList == null) {
            return 0;
        } else {
            return studentSessionJoinList.size();
        }
    }

    static class StudentSessionJoinViewHolder extends RecyclerView.ViewHolder {
        private TextView studentSessionJoinText;

        public StudentSessionJoinViewHolder(View itemView) {
            super(itemView);
            studentSessionJoinText = itemView.findViewById(R.id.studentSessionJoinItem);
        }
    }
}
