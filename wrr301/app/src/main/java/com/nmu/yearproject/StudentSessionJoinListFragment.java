package com.nmu.yearproject;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class StudentSessionJoinListFragment extends Fragment {
    private StudentSessionJoinListAdapter studentSessionJoinListAdapter;
    private StudentSessionJoinViewModel studentSessionJoinViewModel;
    private Context context;

    public static StudentSessionJoinListFragment newInstance() {
        return new StudentSessionJoinListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        studentSessionJoinListAdapter = new StudentSessionJoinListAdapter(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_studentsessionjoin, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_studentsessionjoin);
        recyclerView.setAdapter(studentSessionJoinListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        return view;
    }

    private void initData() {
        studentSessionJoinViewModel = ViewModelProviders.of(this).get(StudentSessionJoinViewModel.class);
        studentSessionJoinViewModel.getStudentSessionJoinList().observe(this, new Observer<List<StudentSessionJoin>>() {
            @Override
            public void onChanged(@Nullable List<StudentSessionJoin> studentSessionJoins) {
                studentSessionJoinListAdapter.setStudentSessionJoinList(studentSessionJoins);
            }
        });
    }

    public void removeData() {
        if (studentSessionJoinViewModel != null) {
            studentSessionJoinViewModel.deleteAll();
        }
    }
}
