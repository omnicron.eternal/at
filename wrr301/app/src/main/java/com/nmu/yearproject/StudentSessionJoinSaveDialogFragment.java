package com.nmu.yearproject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class StudentSessionJoinSaveDialogFragment extends DialogFragment {

    private Context context;
    private String sNumExtra;
    private String sesIDExtra;



    private static final String EXTRA_SNUM = "snum";
    private static final String EXTRA_SESID = "sesID";


    public static final String TAG_DIALOG_STUDENTSESSIONJOIN_SAVE = "dialog_studentSessionJoin_save";

    public static StudentSessionJoinSaveDialogFragment newInstance(String sNum,String sesID) {
        StudentSessionJoinSaveDialogFragment fragment = new StudentSessionJoinSaveDialogFragment();

        Bundle args = new Bundle();
        args.putString(EXTRA_SNUM, sNum);
        args.putString(EXTRA_SESID, sesID);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        sNumExtra = args.getString(EXTRA_SNUM);
        sesIDExtra = args.getString(EXTRA_SESID);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_student_session_join, null);

        final EditText edt_sNum = view.findViewById(R.id.edt_sNumCapture);
        final EditText edt_sesID = view.findViewById(R.id.edt_sesIDCapture);

        if (sNumExtra != null) {
            edt_sNum.setText(sNumExtra);
            edt_sNum.setSelection(sNumExtra.length());
        }
        if (sesIDExtra != null) {
            edt_sesID.setText(sesIDExtra);
            edt_sesID.setSelection(sesIDExtra.length());
        }
        alertDialogBuilder.setView(view)
                .setTitle("Enter new student")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveStudentSessionJoin(edt_sNum.getText().toString(),edt_sesID.getText().toString());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteStudentSessionJoin(edt_sNum.getText().toString(),edt_sesID.getText().toString());
                    }
                });

        return alertDialogBuilder.create();
    }

    private void saveStudentSessionJoin(String sNum,String sesID) {
        if (TextUtils.isEmpty(sNum)) {
            return;
        }
/*
        StudentSessionJoinDao studentSessionJoinDao = appDatabase.getDatabase(context).studentSessionJoinDao();

        if (sNumExtra != null) {
            // clicked on item row -> update
            StudentSessionJoin studentSessionJoinToUpdate = studentSessionJoinDao.findStudentSessionJoin(sNum,sesID);
            if (studentSessionJoinToUpdate != null) {
                if (!(studentSessionJoinToUpdate.sNum.equals(sNum) && studentSessionJoinToUpdate.sesID.equals(sesID))) {
                    studentSessionJoinToUpdate.sesID.;
                    studentDao.update(studentSessionJoinToUpdate);
                }
            }
        }
        else
        {
            */
        StudentSessionJoinDao studentSessionJoinDao = appDatabase.getDatabase(context).studentSessionJoinDao();

        studentSessionJoinDao.insert(new StudentSessionJoin(sNum,sesID));
        //}
    }

    private void deleteStudentSessionJoin(String sNum,String sesID)
    {
        StudentSessionJoinDao studentSessionJoinDao = appDatabase.getDatabase(context).studentSessionJoinDao();
        if (sNumExtra != null && sesIDExtra!=null) {
            StudentSessionJoin studentSessionJoinToDelete =studentSessionJoinDao.findStudentSessionJoin(sNum,sesID);
            studentSessionJoinDao.delete(studentSessionJoinToDelete);
        }


    }
}
