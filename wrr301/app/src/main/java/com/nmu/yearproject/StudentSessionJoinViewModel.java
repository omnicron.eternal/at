package com.nmu.yearproject;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class StudentSessionJoinViewModel extends AndroidViewModel {
    private StudentSessionJoinDao studentSessionJoinDao;
    private LiveData<List<StudentSessionJoin>> studentSessionJoinLiveData;

    public StudentSessionJoinViewModel(@NonNull Application application) {
        super(application);
        studentSessionJoinDao = appDatabase.getDatabase(application).studentSessionJoinDao();
        studentSessionJoinLiveData = studentSessionJoinDao.getAll();
    }

    public LiveData<List<StudentSessionJoin>> getStudentSessionJoinList() {
        return studentSessionJoinLiveData;
    }

    public void insert(StudentSessionJoin... studentSessionJoins) {
        studentSessionJoinDao.insert(studentSessionJoins);
    }

    public void update(StudentSessionJoin studentSessionJoin) {
        studentSessionJoinDao.update(studentSessionJoin);
    }

    public void deleteAll() {
        studentSessionJoinDao.deleteAll();
    }
}

