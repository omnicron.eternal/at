package com.nmu.yearproject;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class StudentViewModel extends AndroidViewModel {
    private StudentDao studentDao;
    private LiveData<List<Student>> studentLiveData;

    public StudentViewModel(@NonNull Application application) {
        super(application);
        studentDao = appDatabase.getDatabase(application).studentDao();
        studentLiveData = studentDao.getAll();
    }

    public LiveData<List<Student>> getStudentList() {
        return studentLiveData;
    }

    public void insert(Student... students) {
        studentDao.insert(students);
    }

    public void update(Student student) {
        studentDao.update(student);
    }

    public void deleteAll() {
        studentDao.deleteAll();
    }
}
