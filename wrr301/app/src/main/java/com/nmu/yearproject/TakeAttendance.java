package com.nmu.yearproject;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.List;

public class TakeAttendance extends AppCompatActivity implements OnClickListener {

    private Button scanBtn;
    private TextView formatTxt;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_attendance_matt);
        context=this;

        scanBtn = (Button)findViewById(R.id.btn_scanCard);
        //formatTxt = (TextView)findViewById(R.id.scan_format);
        //contentTxt = (TextView)findViewById(R.id.scan_content);
        final Spinner spinModule=(Spinner) findViewById(R.id.spn_Attendance_selectModule);
        final Spinner spinSession=(Spinner) findViewById(R.id.spn_Attendance_selectSession);

        scanBtn.setOnClickListener(this);

        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();

        List<Module> moduleList=moduleDao.getAllStatic();
        CustomArrayAdapterModule adapterModule = new CustomArrayAdapterModule(this,R.layout.adapter_spinner_module,moduleList);
        spinModule.setAdapter(adapterModule);

        spinModule.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, final int pos, long id) {

                        //final ListView sesListView= (ListView) findViewById(R.id.listView_sessionSelectByModule);

                        final SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();

                        final List<Session> sessions =sessionDao.findSessionByModuleCode(((Module)spinModule.getSelectedItem()).getModuleCode());
                        //final List<Session> sessions =sessionDao.getAllStatic();

                        final CustomArrayAdapter adapterSession = new CustomArrayAdapter(context,R.layout.adapter_spinner_session,sessions);
                        spinSession.setAdapter(adapterSession);

                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


    }

    public void onClick(View v){
    //respond to clicks
        if(v.getId()==R.id.btn_scanCard){
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();
        //scan
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    //retrieve scan result
    IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
        //we have a result
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();
            //formatTxt.setText("FORMAT: " + scanFormat);
            //contentTxt.setText("CONTENT: " + scanContent);

            Spinner sesId=(Spinner) findViewById(R.id.spn_Attendance_selectSession);

            StudentDao studentDao = appDatabase.getDatabase(getApplicationContext()).studentDao();
            SessionDao sessionDao=appDatabase.getDatabase(getApplicationContext()).sessionDao();
            StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(getApplicationContext()).studentSessionJoinDao();

            Student stu=studentDao.findStudentByStudentNum(scanContent.substring(1,scanContent.length()));
            if (stu!=null)
            {
                String sesID=((Session)sesId.getSelectedItem()).getSessionID();
                Session ses=sessionDao.findSessionByID(sesID);
                if (ses!=null)
                {
                    studentSessionJoinDao.insert(new StudentSessionJoin(stu.getStudentNumber(),sesID));
                }
            }
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
