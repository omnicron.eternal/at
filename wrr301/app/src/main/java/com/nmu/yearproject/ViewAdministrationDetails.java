package com.nmu.yearproject;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

import static com.nmu.yearproject.StudentSaveDialogFragment.TAG_DIALOG_STUDENT_SAVE;

public class ViewAdministrationDetails extends AppCompatActivity {
    private Fragment shownFragment;

    Context context;
    StudentListAdapterStatic studentAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.activity_view_administration_details);

        final Spinner spin=(Spinner) findViewById(R.id.spn_selectModuleAdmin);


        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();

        final Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");

        List<Module> moduleList=moduleDao.getAllStatic();

        CustomArrayAdapterModule adapter = new CustomArrayAdapterModule(this,R.layout.adapter_spinner_module,moduleList);
        spin.setAdapter(adapter);


        spin.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, final int pos, long id) {

                        final ListView sesListView= (ListView) findViewById(R.id.listView_sessionSelectByModule);
                        final StudentModuleJoinDao studentModuleJoinDao=appDatabase.getDatabase(getBaseContext()).studentModuleJoinDao();

                        final List<Student> students =studentModuleJoinDao.getStudentForModuleStatic(((Module)spin.getSelectedItem()).getModuleCode());
                        studentAdapter = new StudentListAdapterStatic(context,R.layout.adapter_student,students);
                        sesListView.setAdapter(studentAdapter);

                        sesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                                new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert)
                                        .setTitle("Delete Record")
                                        .setMessage("Are you sure you want to delete this record")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                String sNum=((Student)sesListView.getItemAtPosition(position)).getStudentNumber();
                                                StudentDao studentDao=appDatabase.getDatabase(getBaseContext()).studentDao();
                                                Student stu =studentDao.findStudentByStudentNum(sNum);
                                                //String ses=((Session)spin.getSelectedItem()).getSessionID();
                                                //studentSessionJoinDao.delete(new StudentSessionJoin(sNum,ses));
                                                studentDao.delete(stu);
                                                studentAdapter.remove(stu);
                                                studentAdapter.notifyDataSetChanged();
                                                //sesListView.
                                                //students.remove(stu);
                                            }})
                                        .setNegativeButton(android.R.string.no, null).show();
                            }
                        });


                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
    }

    public void AddStudentToModule(View view)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert);

        final EditText edt_StudentNumber = new EditText(context);
        edt_StudentNumber.setHint("Student Number");

        alert.setTitle("Enter Student");

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.addView(edt_StudentNumber);

        alert.setView(layout);



        alert.setPositiveButton("Add Student", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                StudentDao studentDao=appDatabase.getDatabase(getBaseContext()).studentDao();
                StudentModuleJoinDao studentModuleJoinDao=appDatabase.getDatabase(getBaseContext()).studentModuleJoinDao();

                Student stu=studentDao.findStudentByStudentNum(edt_StudentNumber.getText().toString());
                if (stu!=null) {
                    final Spinner spin = (Spinner) findViewById(R.id.spn_selectModuleAdmin);

                    Module mod = (Module) spin.getSelectedItem();
                    StudentModuleJoin record = new StudentModuleJoin(stu.getStudentNumber(), mod.getModuleCode());

                    studentModuleJoinDao.insert(record);

                    studentAdapter.add(stu);
                    studentAdapter.notifyDataSetChanged();
                    //modListView.refreshDrawableState();
                    //modListView.invalidate();
                }
                else edt_StudentNumber.setText("No student with that student number");
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }
}

/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_administration_details);
        initView();
        if (savedInstanceState == null) {
            showFragment(StudentListFragment.newInstance());
        }

    }

    private void initView() {
        showFragment(StudentListFragment.newInstance());

        Button btn_addStudent = findViewById(R.id.btn_addStudent);
        btn_addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveDialog();
            }
        });
    }

    private void showFragment(final Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentHolderStudent, fragment);
        fragmentTransaction.commitNow();
        shownFragment = fragment;
    }

    private void showSaveDialog() {
        DialogFragment dialogFragment;
        String tag;

        dialogFragment = StudentSaveDialogFragment.newInstance(null, null, null, null,null,null,null);
        tag = TAG_DIALOG_STUDENT_SAVE;
        dialogFragment.show(getSupportFragmentManager(), tag);
    }
    */

