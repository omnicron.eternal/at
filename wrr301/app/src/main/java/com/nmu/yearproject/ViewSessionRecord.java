package com.nmu.yearproject;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

public class ViewSessionRecord extends AppCompatActivity {

    Context context;
    StudentListAdapterStatic studentAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.activity_view_session_record);

        final Spinner spin=(Spinner) findViewById(R.id.spn_selectSession);


        SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();

        final Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");

        List<Session> sessionList=sessionDao.getAllStatic();

        CustomArrayAdapter adapter = new CustomArrayAdapter(this,R.layout.adapter_spinner_session,sessionList);
        spin.setAdapter(adapter);


        spin.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, final int pos, long id) {

                        final ListView sesListView= (ListView) findViewById(R.id.listView_sessionSelect);
                        final StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(getBaseContext()).studentSessionJoinDao();

                        final List<Student> students =studentSessionJoinDao.getStudentForSessionStatic(((Session)spin.getSelectedItem()).getSessionID());
                        studentAdapter = new StudentListAdapterStatic(context,R.layout.adapter_student,students);
                        sesListView.setAdapter(studentAdapter);

                        sesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                                new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert)
                                        .setTitle("Delete Record")
                                        .setMessage("Are you sure you want to delete this record")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                String sNum=((Student)sesListView.getItemAtPosition(position)).getStudentNumber();
                                                StudentDao studentDao=appDatabase.getDatabase(getBaseContext()).studentDao();
                                                Student stu =studentDao.findStudentByStudentNum(sNum);
                                                String ses=((Session)spin.getSelectedItem()).getSessionID();
                                                studentSessionJoinDao.delete(new StudentSessionJoin(sNum,ses));

                                                studentAdapter.remove(stu);
                                                studentAdapter.notifyDataSetChanged();
                                                //sesListView.
                                                //students.remove(stu);
                                            }})
                                        .setNegativeButton(android.R.string.no, null).show();
                            }
                        });


                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
    }
    public void AddRecord(View view)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context,R.style.Theme_AppCompat_Dialog_Alert);

        final EditText edt_StudentNumber = new EditText(context);
        edt_StudentNumber.setHint("Student Number");

        alert.setTitle("Enter Student");

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.addView(edt_StudentNumber);

        alert.setView(layout);



        alert.setPositiveButton("Add Student", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                StudentDao studentDao=appDatabase.getDatabase(getBaseContext()).studentDao();
                StudentSessionJoinDao studentSessionJoinDao=appDatabase.getDatabase(getBaseContext()).studentSessionJoinDao();

                Student stu=studentDao.findStudentByStudentNum(edt_StudentNumber.getText().toString());
                final Spinner spin=(Spinner) findViewById(R.id.spn_selectSession);

                Session ses=(Session)spin.getSelectedItem();
                StudentSessionJoin record=new StudentSessionJoin(stu.getStudentNumber(),ses.getSessionID());

                studentSessionJoinDao.insert(record);

                studentAdapter.add(stu);
                studentAdapter.notifyDataSetChanged();
                //modListView.refreshDrawableState();
                //modListView.invalidate();

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }


}
