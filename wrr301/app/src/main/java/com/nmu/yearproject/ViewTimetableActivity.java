package com.nmu.yearproject;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class ViewTimetableActivity extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_view_timetable);

        ModuleDao moduleDao = appDatabase.getDatabase(getBaseContext()).moduleDao();
        StudentModuleJoinDao studentModuleJoinDao = appDatabase.getDatabase(getBaseContext()).studentModuleJoinDao();
        SessionDao sessionDao = appDatabase.getDatabase(getBaseContext()).sessionDao();

        ListView sesListView = (ListView) findViewById(R.id.listView_TimeTable);


        final Student stu = (Student) getIntent().getSerializableExtra("EXTRA_STUDENT");

        List<Module> moduleList = studentModuleJoinDao.getModuleForStudentStatic(stu.getStudentNumber());

        List<Session> sessionList = new ArrayList<Session>();
        for (Module module : moduleList) {
            List<Session> tempSesList = sessionDao.findSessionByModuleCode(module.getModuleCode());
            sessionList.addAll(tempSesList);
        }

        SessionListAdapterStatic adapter = new SessionListAdapterStatic(this, R.layout.adapter_session, sessionList);
        sesListView.setAdapter(adapter);

        if (sesListView.getHeaderViewsCount()==0)
        {
            View headerView = getLayoutInflater().inflate(R.layout.header_session, null);
            sesListView.addHeaderView(headerView);
        }

    }
}
