package com.nmu.yearproject;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

public class ViewTrends extends AppCompatActivity {
    Context context;
    SessionTrendAdapter adapterSession;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_trends);
        context=this;
        final Spinner spin=(Spinner) findViewById(R.id.spn_selectModuleTrends);


        ModuleDao moduleDao=appDatabase.getDatabase(getBaseContext()).moduleDao();

        final Student stu=(Student)getIntent().getSerializableExtra("EXTRA_STUDENT");

        List<Module> moduleList=moduleDao.getAllStatic();

        CustomArrayAdapterModule adapter = new CustomArrayAdapterModule(this,R.layout.adapter_spinner_module,moduleList);
        spin.setAdapter(adapter);




        spin.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, final int pos, long id) {

                        final ListView sesListView= (ListView) findViewById(R.id.listView_sessionTrend);
                        final SessionDao sessionDao=appDatabase.getDatabase(getBaseContext()).sessionDao();

                        final List<Session> sessions =sessionDao.findSessionByModuleCode(((Module)spin.getSelectedItem()).getModuleCode());

                        adapterSession = new SessionTrendAdapter(context,R.layout.adapter_session_trend,sessions);
                        sesListView.setAdapter(adapterSession);


                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


    }




}
