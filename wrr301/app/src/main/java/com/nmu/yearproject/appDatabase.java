package com.nmu.yearproject;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

// User and Book are classes annotated with @Entity.
@Database(version = 8, entities = {Student.class, Session.class,Module.class,StudentSessionJoin.class,StudentModuleJoin.class})
public abstract class appDatabase extends RoomDatabase {
    private static appDatabase INSTANCE;
    private static final String DB_NAME = "appDatabase.db";

    public static appDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (appDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            appDatabase.class, DB_NAME).allowMainThreadQueries() // SHOULD NOT BE USED IN PRODUCTION !!!
                            .addCallback(new RoomDatabase.Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Log.d("appDatabase", "populating with data...");
                                    new PopulateDbAsync(INSTANCE).execute();
                                }
                            }).fallbackToDestructiveMigration()
                            .build();
                }
            }
        }

        return INSTANCE;
    }
    public void clearDb() {
        if (INSTANCE != null) {
            new PopulateDbAsync(INSTANCE).execute();
        }
    }


    abstract public StudentDao studentDao();

    abstract public ModuleDao moduleDao();

    abstract public SessionDao sessionDao();

    abstract public StudentSessionJoinDao studentSessionJoinDao();

    abstract public StudentModuleJoinDao studentModuleJoinDao();


    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final StudentDao studentDao;
        private final ModuleDao moduleDao;
        private final SessionDao sessionDao;
        private final StudentSessionJoinDao studentSessionJoinDao;
        private final StudentModuleJoinDao studentModuleJoinDao;

        public PopulateDbAsync(appDatabase instance) {
            studentDao = instance.studentDao();
            moduleDao = instance.moduleDao();
            sessionDao=instance.sessionDao();
            studentSessionJoinDao=instance.studentSessionJoinDao();
            studentModuleJoinDao=instance.studentModuleJoinDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            studentDao.deleteAll();
            moduleDao.deleteAll();
            sessionDao.deleteAll();
            studentSessionJoinDao.deleteAll();
            studentModuleJoinDao.deleteAll();

            /*
            Student dylanPhelps =new Student("216233046","Dylan","Phelps","s216233046@mandela.ac.za","123",true,false);
            Student mattWatson =new Student("215100328","Matt","Watson","s215100328@mandela.ac.za","abc",false,true);
            Student isabelleTaljaard =new Student("216087686","Isabelle","Taljaard","s216087686@mandela.ac.za","123",false,false);
            Student adamKhan =new Student("216302420","Adam","Khan","s216302420@mandela.ac.za","abc",false,false);


            Module matt302=new Module("MATT302","Modern Algebra","Study of abstract algebraic structures","Science");
            Module wrlv301=new Module("WRLV301","Formal Language and Automata Theory","FLAT","Science");
            Module wrui301=new Module("wrui301","User Interface Design","UI Design","Science");

            Session matt302a1=new Session("MATT302001","MATT302","07:45","09:05","2018/09/11","0702025",Boolean.TRUE,"YES");
            Session wrlv301b2=new Session("WRLV301123","WRLV301","10:25","11:45","2018/08/19","3001017",Boolean.TRUE,"NO");


            studentDao.insert(dylanPhelps,mattWatson,isabelleTaljaard,adamKhan);
            moduleDao.insert(matt302,wrlv301,wrui301);
            sessionDao.insert(matt302a1,wrlv301b2);

            studentModuleJoinDao.insert(new StudentModuleJoin("216233046","matt302"));
            studentModuleJoinDao.insert(new StudentModuleJoin("216233046","wrlv301"));
            studentModuleJoinDao.insert(new StudentModuleJoin("216233046","wrui301"));



            studentSessionJoinDao.insert(new StudentSessionJoin("216233046","MATT302001"));
            studentSessionJoinDao.insert(new StudentSessionJoin("216233046","WRLV301123"));
            studentSessionJoinDao.insert(new StudentSessionJoin("215100328","MATT302001"));

            */


            return null;
        }
    }

}
